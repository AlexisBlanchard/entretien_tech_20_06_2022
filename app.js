const Utils = require('./utils.js');

class Fraction
{
    numerator;
    denominator;

    constructor(num, deno)
    {
        if (deno == 0)
            throw new Error("A fraction denominator cannot be equal to 0");
        this.numerator = num;
        this.denominator = deno;
    }

    multiply(fraction)
    {
        return new Fraction(this.numerator * fraction.numerator, this.denominator * fraction.denominator);
    }

    divide(fraction)
    {
        return this.multiply(new Fraction(fraction.denominator, fraction.numerator));
    }

    add(fraction)
    {
        const a = this.multiply(new Fraction(fraction.denominator, fraction.denominator));
        const b = fraction.multiply(new Fraction(this.denominator, this.denominator));

        return new Fraction(a.numerator + b.numerator, this.denominator * fraction.denominator).simplify();
    }

    substract(fraction)
    {
        return this.add(new Fraction(-fraction.numerator, fraction.denominator));
    }

    simplify()
    {
        const pgcd = Utils.pgcd(this.numerator, this.denominator);
        return new Fraction(this.numerator / pgcd, this.denominator / pgcd);
    }
}

const a = new Fraction(20, 15);
const b = new Fraction(18, 30);

// 4/5 (coef = 5) ; 3/5 (coef = 6)
console.log(a.simplify(), b.simplify());

// 58/30 => 29/15 (coef = 2)
console.log(a.add(b), a.substract(b).simplify());

// 22/30 => 11/15 (coef = 2)
console.log(a.substract(b), a.substract(b).simplify());

// 360/450
console.log(a.multiply(b), a.multiply(b).simplify());

// 600/270
console.log(a.divide(b), a.divide(b).simplify());

/** Uncomment to generate an error.
  * This error is generated in the constructor method of the Fraction class
  */
// const error = new Fraction(200, 0);